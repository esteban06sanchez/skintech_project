<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Rutas para categorías
Route::get('/lista-categoria', 'categoryController@index')->name('categoria.index');
Route::get('/categoria', 'categoryController@create')->name('categoria.create');
Route::post('/categorias/crear', 'categoryController@store')->name('categoria.store');
Route::get('/categorias/editar/{id_category}', 'categoryController@edit')->name('categoria.edit');
Route::put('/categorias/{id_category}', 'categoryController@update')->name('categoria.update');
Route::delete('/categorias/eliminar/{id_category}', 'categoryController@destroy')->name('categoria.destroy');


//Rutas para subcategorias
Route::get('/lista-subcategoria', 'subCategoryController@index')->name('subcategoria.index');
Route::get('/subcategoria', 'subCategoryController@create')->name('subcategoria.create');
Route::post('/subcategorias/crear', 'subCategoryController@store')->name('subcategoria.store');
Route::get('/subcategorias/editar/{id_subcategory}', 'subCategoryController@edit')->name('subcategoria.edit');
Route::put('/subcategorias/{id_subcategory}', 'subCategoryController@update')->name('subcategoria.update');
Route::delete('/subcategorias/eliminar/{id_subcategory}', 'subCategoryController@destroy')->name('subcategoria.destroy');



//Rutas para productos
Route::get('/lista-productos', 'productController@index')->name('productos.index');
Route::get('/productos', 'productController@create')->name('productos.create');
Route::post('/productos/crear', 'productController@store')->name('product.store');
Route::get('/productos/editar/{id_product}', 'productController@edit')->name('product.edit');
Route::put('/productos/{id_product}', 'productController@update')->name('product.update');
Route::delete('/productos/eliminar/{id_product}', 'productController@destroy')->name('product.destroy');


//Rutas para usuarios
Route::get('/usuarios', 'userController@create')->name('usuarios.create');
Route::get('/lista-usuarios', 'userController@index')->name('usuarios.index');
Route::post('/usuarios/crear', 'userController@store')->name('usuarios.store');

//Rutas para asociar
Route::get('/getCategories', 'Services@getCategories')->name('getCategories');
Route::get('/asociar-producto/crear', 'Services@create')->name('asociarProducto.create');
Route::post('/asociar-producto', 'Services@store')->name('asociarProducto.store');


//Rutas principales
Route::view('/', 'login');

