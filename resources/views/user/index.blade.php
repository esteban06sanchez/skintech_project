@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-12 container my-3 border">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap" width="100%">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombre Usuario</th>
                          <th>Rol Usuario</th>
                          <th>Estado</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>183</td>
                          <td>John Doe</td>
                          <td>11-7-2014</td>
                          <td><span class="tag tag-success">Approved</span></td>
                          <td><i class="fas fa-trash-alt" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></td>
                          <td><i class="fas fa-edit" data-toggle="tooltip" data-placement="top" title="Editar"></i></td>
                        </tr>
                        <tr>
                          <td>219</td>
                          <td>Alexander Pierce</td>
                          <td>11-7-2014</td>
                          <td><span class="tag tag-warning">Pending</span></td>
                          <td><i class="fas fa-trash-alt" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></td>
                          <td><i class="fas fa-edit" data-toggle="tooltip" data-placement="top" title="Editar"></i></td>
                        </tr>
                        <tr>
                          <td>657</td>
                          <td>Bob Doe</td>
                          <td>11-7-2014</td>
                          <td><span class="tag tag-primary">Approved</span></td>
                          <td><i class="fas fa-trash-alt" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></td>
                          <td><i class="fas fa-edit" data-toggle="tooltip" data-placement="top" title="Editar"></i></td>
                        </tr>
                        <tr>
                          <td>175</td>
                          <td>Mike Doe</td>
                          <td>11-7-2014</td>
                          <td><span class="tag tag-danger">Denied</span></td>
                          <td><i class="fas fa-trash-alt" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></td>
                          <td><i class="fas fa-edit" data-toggle="tooltip" data-placement="top" title="Editar"></i></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
