@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-6 container my-3">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Crear nuevo usuario</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{route('usuarios.store')}}">
                    @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label>Nombre Usuario</label>
                      <input type="text" class="form-control" name="name_user" placeholder="Ej: William Sanchez">
                      @isset($errors)
                         @foreach ($errors->all() as $error)
                            <p class="text-danger">{{$error}}</p>
                        @endforeach
                      @endisset
                    </div>
                    <div class="form-group">
                      <label>Admin</label>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="role_user">
                      </div>
                    </div>
                    <div class="form-group">
                        <label>Activo</label>
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" name="state_user">
                        </div>
                      </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary my-2">Agregar</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->

            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
