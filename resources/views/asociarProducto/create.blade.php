@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-6 container my-3">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Asociar productos</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('asociarProducto.store') }}">
                    @csrf
                  <div class="card-body">
                    <div class="form-group">
                        <label for="id_product">Nombre Producto</label>
                        <select class="select2 form-control" name="id_product" id="id_product">
                            @forelse ($Products as $product)
                                <option value="{{$product->id_product}}">{{$product->name_product}}</option>
                            @empty
                                <option value="0">Sin opciones</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="id_category">Categoría</label>
                        <select class="custom-select" name="id_category" id="id_category">
                            @forelse ($Category as $item)
                                <option value="{{$item->id_category}}">{{$item->name_category}}</option>
                            @empty
                                <option value="0">Sin opciones</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="id_subcategory">Subcategoría</label>
                        <select class="custom-select" name="id_subcategory" id="id_subcategory">
                            {{-- @forelse ($Subcategory as $item)
                                <option value="{{$item->id_subcategory}}">{{$item->name_subcategory}}</option>
                            @empty
                                <option value="0">Sin opciones</option>
                            @endforelse --}}
                        </select>
                        @isset($errors)
                          @foreach ($errors->all() as $error)
                              <p class="text-danger">{{$error}} </p>
                          @endforeach
                        @endisset
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary my-2">Guardar</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection

@section('functionsJS')
    <script>
        $('.select2').select2()

        var categories = document.getElementById('id_category')
        var subcategories = document.getElementById('id_subcategory')

        function listarSubCategories(){
            fetch('http://skintech.test/getCategories')
            .then(function(response) {
                return response.json()
            })
            .then(function(myJson) {
                subcategories.innerHTML = ''
                myJson.forEach(element => {
                    if(element.id_category == categories.value){
                        subcategories.innerHTML += '<option value="' +
                            element.id_subcategory + '">' +
                            element.name_subcategory + '</option>'
                    }
                });
            })
        }

        listarSubCategories()

        categories.addEventListener('change', function(){
            listarSubCategories()
        })
    </script>
@endsection
