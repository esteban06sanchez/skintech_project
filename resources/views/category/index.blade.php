@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-12 container my-3 border">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap" width="100%">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombre Categoria</th>
                          <th>Estado Categoria</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($Category as $item)
                              <tr>
                                <td>{{$item->id_category}}</td>
                                <td>{{$item->name_category}}</td>
                                <td>{{$item->state_category}}</td>
                                <td ><a href="{{ route('categoria.edit', $item) }}" class="btn btn-warning btn-sm">Editar</a></td>
                                <td>
                                    <form action="{{ route('categoria.destroy', $item->id_category) }}" method="POST">
                                        @csrf @method('delete')
                                        <button class="btn btn-danger btn-sm">Eliminar</button>
                                    </form>
                                </td>
                              </tr>
                          @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
