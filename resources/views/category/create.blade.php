@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-6 container my-3">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Agregar Categoria</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{route('categoria.store') }}">
                    @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label>Nombre Categoria</label>
                      <input type="text" class="form-control" name="name_category"placeholder="Ej: Redes, computadores, celulares...">
                      @isset($errors)
                         @foreach ($errors->all() as $error)
                            <p class="text-danger">{{$error}}</p>
                        @endforeach
                      @endisset
                    </div>
                    <div class="form-group">
                      <label>Activo</label>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="state_category">
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary my-2">Agregar</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
