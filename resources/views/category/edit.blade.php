@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-6 container my-3">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Editar Categoria</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
            <form action="{{route('categoria.update', $category)}}" method="POST">
                    @method('PUT')
                    @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label>Nombre Categoria</label>
                      <input type="text" class="form-control" name="name_category"
                          placeholder="Ej: Redes, computadores, celulares..." value="{{$category->name_category}}">
                    </div>
                    <div class="form-group">
                      <label>Activo</label>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="state_category" {{ $category->state_category == 'Activo' ? 'checked' : '' }}>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary my-2">Editar</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
