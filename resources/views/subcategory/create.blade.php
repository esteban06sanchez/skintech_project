@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-6 container my-3">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Agregar Subcategoria</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{route('subcategoria.store') }}">
                    @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nombre Subcategoria</label>
                      <input type="text" class="form-control" name="name_subcategory"placeholder="Ej: Accesorios, Cargadores, pantallas...">
                      @isset($errors)
                         @foreach ($errors->all() as $error)
                            <p class="text-danger">{{$error}}</p>
                        @endforeach
                      @endisset
                    </div>
                    <div class="form-group">
                        <label for="id_category">Categoría</label>
                        <select class="custom-select" name="id_category" id="id_category">
                            @forelse ($Category as $item)
                                <option value="{{$item->id_category}}">{{$item->name_category}}</option>
                            @empty
                                <option value="0">Sin opciones</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Activo</label>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="state_subcategory" value="1" checked>
                      </div>
                    </div>

                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary my-2">Agregar</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->

            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
