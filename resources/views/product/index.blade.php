@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-12 container my-3 border">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap" width="100%">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombre Producto</th>
                          <th>Categoria</th>
                          <th>Subcategoria</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($Product as $item)
                            <tr>
                                <td>{{ $item->id_product }}</td>
                                <td>{{ $item->name_product }}</td>
                                <td>{{ $item->name_category }}</td>
                                <td>{{ $item->name_subcategory }}</td>
                                <td>
                                    <form method="POST" action="{{ route('product.destroy', $item->id_product) }}">
                                        @csrf @method('delete')
                                        <input type="hidden" name="id_subcategory" value="{{ $item->id_subcategory }}">
                                        <button class="btn btn-danger btn-sm">Eliminar</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
