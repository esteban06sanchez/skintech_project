@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-6 container my-3">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Crear producto</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('product.store') }}">
                    @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nombre Producto</label>
                      <input type="text" name="name_product" class="form-control" placeholder="Ej: Accesorios, Cargadores, pantallas...">
                      @isset($errors)
                          @foreach ($errors->all() as $error)
                              <p class="text-danger">{{$error}} </p>
                          @endforeach
                      @endisset
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary my-2">Crear Producto</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    {{-- <script>

        var categories = document.getElementById('id_category')
        var subcategories = document.getElementById('id_subcategory')

        function listarSubCategories(){
            fetch('http://skintech.test/getCategories')
            .then(function(response) {
                return response.json()
            })
            .then(function(myJson) {
                subcategories.innerHTML = ''
                myJson.forEach(element => {
                    if(element.id_category == categories.value){
                        subcategories.innerHTML += '<option value="' +
                            element.id_subcategory + '">' +
                            element.name_subcategory + '</option>'
                    }
                });
            })
        }

        listarSubCategories()

        categories.addEventListener('change', function(){
            listarSubCategories()
        })
    </script> --}}
@endsection
