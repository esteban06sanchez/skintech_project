@extends('admin.layout')

@section('cuerpo')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row ">
            <!-- left column -->
            <div class="col-md-6 container my-3">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Asociar producto</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{route('product.update', $product)}}">
                    @method('PUT')
                    @csrf
                  <div class="card-body">
                    <div class="form-group">
                      <label>Nombre Producto</label>
                      <input type="text" name="name_product" class="form-control"
                            placeholder="Ej: Accesorios, Cargadores, pantallas..." value="{{$product->name_product}}">
                      @isset($errors)
                          @foreach ($errors->all() as $error)
                              <p class="text-danger">{{$error}} </p>
                          @endforeach
                      @endisset
                    </div>
                    <div class="form-group">
                        <label for="id_category">Categoría</label>
                        <select class="custom-select" name="id_category" id="id_category">
                            @forelse ($Category as $item)
                                <option value="{{$item->id_category}}">{{$item->name_category}}</option>
                            @empty
                                <option value="0">Sin opciones</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="id_subcategory">Subcategoría</label>
                        <select class="custom-select" name="id_subcategory" id="id_subcategory">
                            {{-- @forelse ($Subcategory as $item)
                                <option value="{{$item->id_subcategory}}">{{$item->name_subcategory}}</option>
                            @empty
                                <option value="0">Sin opciones</option>
                            @endforelse --}}
                        </select>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary my-2">Crear Producto</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script>

        var categories = document.getElementById('id_category')
        var subcategories = document.getElementById('id_subcategory')

        function listarSubCategories(){
            fetch('http://skintech.test/getCategories')
            .then(function(response) {
                return response.json()
            })
            .then(function(myJson) {
                subcategories.innerHTML = ''
                myJson.forEach(element => {
                    if(element.id_category == categories.value){
                        subcategories.innerHTML += '<option value="' +
                            element.id_subcategory + '">' +
                            element.name_subcategory + '</option>'
                    }
                });
            })
        }

        listarSubCategories()

        categories.addEventListener('change', function(){
            listarSubCategories()
        })
    </script>
@endsection
