<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'subcategory';
    protected $primaryKey = 'id_subcategory';

    protected $fillable = ['name_subcategory', 'id_category', 'state_subcategory'];
}
