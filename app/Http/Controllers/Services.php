<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcategory;
use App\Category;
use App\Product;
use App\SubCategory_Product;

class Services extends Controller
{
    public function getCategories(){
        return Subcategory::where('state_subcategory','=','Activo')->get();
    }

    public function create(){
        return view('asociarProducto.create', [
            'Category' => Category::where('state_category', '=', 'Activo')->get(),
            'Subcategory' => Subcategory::where('state_subcategory','=', 'Activo')->get(),
            'Products' => Product::get()
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'id_product' => 'required',
            'id_subcategory' => 'required'
        ],[
            'id_product.required' => 'Debe seleccionar un producto',
            'id_subcategory.required' => 'Debe seleccionar una subcategoría'
        ]);

        SubCategory_Product::create([
            'id_product' => $request->get('id_product'),
            'id_subcategory' => $request->get('id_subcategory')
        ]);

        return redirect()->route('productos.index');
    }
}
