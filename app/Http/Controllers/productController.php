<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Subcategory;
use App\SubCategory_Product;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Product = Product::join('subcategory_products','products.id_product','=','subcategory_products.id_product')
            ->join('subcategory','subcategory.id_subcategory','=','subcategory_products.id_subcategory')
            ->join('category','category.id_category','=','subcategory.id_category')
            ->select('products.id_product','products.name_product','name_category','name_subcategory','subcategory.id_subcategory')
            ->get();

        return view('product.index', [
            'Product' => $Product
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create', [
            'Category' => Category::where('state_category', '=', 'Activo')->get(),
            'Subcategory' => Subcategory::where('state_subcategory','=', 'Activo')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_product' => 'required'
        ],[
            'name_product.required' => 'El campo nombre es obligatorio'
        ]);

        Product::create([
            'name_product' => $request->get('name_product')
        ]);

        /* $Product = Product::orderBy('id_product', 'desc')->limit(1)->get();

        SubCategory_Product::create([
            'id_subcategory' => $request->get('id_subcategory'),
            'id_product' => $Product[0]->id_product
        ]); */

        return redirect()->route('productos.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_product)
    {
        $product = Product::findOrFail($id_product);
        $Category = Category::where('state_category', '=', 'Activo')->get();
        return view ('product.edit', compact('product', 'Category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Product $id_product)
    {
        //$product = Product::findOrFail($id_product);
        $id_product->update([
             'name_product' => $request->get('name_product')
        ]);

        return redirect()->route('productos.index');

        /* SubCategory_Product::where('');

         return $id_product->id_product; */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_product)
    {
        /* $product = Product::findOrFail($id_product);
        $product->delete();
        return redirect()->route('productos.index'); */

        $EliminarRelacion = SubCategory_Product::where('id_product','=',$id_product)->where('id_subcategory','=',request('id_subcategory'))->delete();
        return redirect()->route('productos.index');
    }
}
