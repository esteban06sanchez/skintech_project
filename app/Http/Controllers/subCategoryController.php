<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcategory;
use App\Category;

class subCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subcategory.index',[
            'Subcategory' => Subcategory::join('category','subcategory.id_category','=','category.id_category')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subcategory.create', [
            'Category' => Category::where('state_category','=','Activo')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'name_subcategory' => 'required'
      ],[
          'name_subcategory.required' => 'El campo nombre es obligatorio'
      ]);

      Subcategory::create([
        'name_subcategory' => $request->get('name_subcategory'),
        'state_subcategory' => empty($request->get('state_subcategory')) ? 'Inactiva' : 'Activo',
        'id_category' => $request->get('id_category')
      ]);

      return redirect()->route('subcategoria.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_subcategory)
    {
        $subcategory = Subcategory::findOrFail($id_subcategory);
        $Category = Category::where('state_category','=','Activo')->get();
        return view ('subcategory.edit', compact('subcategory','Category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcategory $id_subcategory)
    {

        $id_subcategory->update([
            'name_subcategory' => $request->get('name_subcategory'),
            'id_category' => $request->get('id_category'),
            'state_subcategory' => empty($request->get('state_subcategory')) ? 'Inactiva' : 'Activo',
        ]);

        return redirect()->route('subcategoria.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_subcategory)
    {
        $subcategory = Subcategory::findOrFail($id_subcategory);
        $subcategory->delete();
        return redirect()->route('subcategoria.index');
    }
}
