<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory_Product extends Model
{
    protected $table = 'subcategory_products';
    protected $guarded = [];
}
