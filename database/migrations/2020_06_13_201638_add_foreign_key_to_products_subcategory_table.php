<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyToProductsSubcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subcategory_products', function (Blueprint $table) {
            $table->foreign('id_subcategory')->references('id_subcategory')->on('subcategory');
            $table->foreign('id_product')->references('id_product')->on('products');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subcategory_products', function (Blueprint $table) {
            $table->dropForeign('subcategory_products_id_subcategory_foreign');
            $table->dropForeign('subcategory_products_id_product_foreign');
        });
    }
}
